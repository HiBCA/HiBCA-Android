package com.bca.hibca.assistant;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bca.hibca.R;
import com.bca.hibca.util.BCAContants;
import com.bca.hibca.util.Log;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Locale;


public class AssistantActivity extends AppCompatActivity implements MessageDialogFragment.Listener, TextToSpeech.OnInitListener {

    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 1;
    private static final String FRAGMENT_MESSAGE_DIALOG = "message_dialog";
    private static final String STATE_RESULTS = "results";
    private SpeechService mSpeechService;
    private VoiceRecorder mVoiceRecorder;

    // Resource caches
    private int mColorHearing;
    private int mColorNotHearing;

    // View references
    private TextView mStatus;
    private TextView mText;
    private ImageView imageView;

    private TextView txtSpeechInput;
    private ImageButton btnSpeak;
    private final int REQ_CODE_SPEECH_INPUT = 100;

    private Context _context;

    private TextToSpeech textToSpeech;
    private OkHttpClient client = new OkHttpClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle("Hi " + BCAContants.ACCOUNT.fullname);

        textToSpeech = new TextToSpeech(this, this);

        Locale locale = new Locale("in", "ID");
        Locale.setDefault(locale);

        _context = this;
        setContentView(R.layout.activity_assistant);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        final Resources resources = getResources();
        final Resources.Theme theme = getTheme();
        mColorHearing = ResourcesCompat.getColor(resources, R.color.status_hearing, theme);
        mColorNotHearing = ResourcesCompat.getColor(resources, R.color.status_not_hearing, theme);

        txtSpeechInput = (TextView) findViewById(R.id.txtSpeechInput);
        btnSpeak = (ImageButton) findViewById(R.id.btnSpeak);

        // hide the action bar
//		getActionBar().hide();

        btnSpeak.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                promptSpeechInput();
            }
        });

//        mp = MediaPlayer.create(_context, R.raw.promo);

        {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog_voice, null);
            dialogBuilder.setView(dialogView);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                dialogBuilder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        try {
                            //                        if (mp.isPlaying()) {
                            //                            mp.stop();
                            //                        }

                            //                        stopVoiceRecorder();
                            //
                            //                        // Stop Cloud Speech API
                            //                        mSpeechService.removeListener(mSpeechServiceListener);
                            //                        unbindService(mServiceConnection);
                            //                        mSpeechService = null;
                        } catch (Exception ex) {

                        }
                    }
                });
            }

            mStatus = (TextView) dialogView.findViewById(R.id.status);
            mText = (TextView) dialogView.findViewById(R.id.text);
            imageView = (ImageView) dialogView.findViewById(R.id.imageView);

            mStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        //SpeakOut
                        JSONObject jsonObject = new JSONObject(doGetRequestKurs());
                        speakOut(jsonObject.getString("message"));
                        Log.i("masukk");
                    } catch (Exception ex) {
                        Log.e(ex.getMessage(), ex);
                        ex.printStackTrace();
                    }
                }
            });

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (textToSpeech != null)
                        textToSpeech.stop();

                }
            });
            dialogBuilder.setNegativeButton("Tutup", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // Stop listening to voice
                    alertDialog.dismiss();

                    try {
//                        if (mp.isPlaying()) {
//                            mp.stop();
//                        }

//                        stopVoiceRecorder();
//
//                        // Stop Cloud Speech API
//                        mSpeechService.removeListener(mSpeechServiceListener);
//                        unbindService(mServiceConnection);
//                        mSpeechService = null;
                    } catch (Exception ex) {

                    }
                }
            });

            alertDialog = dialogBuilder.create();
        }
    }

    /**
     * Showing google speech input dialog
     */
    private void promptSpeechInput() {
        // Start listening to voices
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_GRANTED) {
            dialogVoice();
//            startVoiceRecorder();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.RECORD_AUDIO)) {
            showPermissionMessageDialog();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO},
                    REQUEST_RECORD_AUDIO_PERMISSION);
        }

    }

    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder binder) {
            mSpeechService = SpeechService.from(binder);
            mSpeechService.addListener(mSpeechServiceListener);
            mStatus.setVisibility(View.VISIBLE);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mSpeechService = null;
        }

    };

//    private MediaPlayer mp;

    private final SpeechService.Listener mSpeechServiceListener =
            new SpeechService.Listener() {
                @Override
                public void onSpeechRecognized(final String text, final boolean isFinal) {
                    if (isFinal) {
                        mVoiceRecorder.dismiss();
                    }
                    if (mText != null && !TextUtils.isEmpty(text)) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (isFinal) {
                                    mText.setText(null);
                                    System.out.println("FAN | mSpeechServiceListener - mText : " + text);

                                    if (
                                            text.toLowerCase().equalsIgnoreCase("promo")
                                                    || text.toLowerCase().equalsIgnoreCase("mau promo dong")
                                                    || text.toLowerCase().equalsIgnoreCase("promo apa aja")
                                                    || text.toLowerCase().equalsIgnoreCase("promo dong")
                                                    || text.toLowerCase().equalsIgnoreCase("ada promo")
                                            ) {
//                                        if (mp.isPlaying()) {
//                                            mp.stop();
//                                        }
//                                        mp = MediaPlayer.create(_context, R.raw.promo);
//                                        mp.start();
                                    } else if (text.toLowerCase().contains("kurs")) {
//                                        if (mp.isPlaying()) {
//                                            mp.stop();
//                                        }
//                                        mp = MediaPlayer.create(_context, R.raw.kurs);

                                        try {
                                            //SpeakOut
                                            JSONObject jsonObject = new JSONObject(doGetRequestKurs());
//                                            Log.i(jsonObject.toString());
//                                            JSONArray jsonArray = jsonObject.getJSONArray("message");
//                                            for (int index = 0; index < jsonArray.length(); index++) {
//                                                speakOut(jsonArray.getString(index));
//                                            }
                                            speakOut(jsonObject.getString("message"));
                                            Log.i("masukk");
                                        } catch (Exception ex) {
                                            Log.e(ex.getMessage(), ex);
                                            ex.printStackTrace();
                                        }

//                                        mp.start();
                                    } else if (text.equalsIgnoreCase("stop")
                                            || text.equalsIgnoreCase("top")
                                            || text.equalsIgnoreCase("berhenti")
                                            ) {
//                                        mp.stop();
                                        if (textToSpeech != null)
                                            textToSpeech.stop();
                                    }
                                } else {
                                    mText.setText(text);
                                }
                            }
                        });
                    }
                }
            };


    public String doGetRequestKurs() throws IOException {
        Log.i("Hit service");
        String url = "http://182.16.165.72/HiBCAService/service/getRate?currency=USD,SGD,GBP";
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }


    private final VoiceRecorder.Callback mVoiceCallback = new VoiceRecorder.Callback() {

        @Override
        public void onVoiceStart() {
            showStatus(true);
            if (mSpeechService != null) {
                mSpeechService.startRecognizing(mVoiceRecorder.getSampleRate());
            }
        }

        @Override
        public void onVoice(byte[] data, int size) {
            if (mSpeechService != null) {
                mSpeechService.recognize(data, size);
            }
        }

        @Override
        public void onVoiceEnd() {
            showStatus(false);
            if (mSpeechService != null) {
                mSpeechService.finishRecognizing();
            }
        }

    };


    private void showPermissionMessageDialog() {
        MessageDialogFragment
                .newInstance(getString(R.string.permission_message))
                .show(getSupportFragmentManager(), FRAGMENT_MESSAGE_DIALOG);
    }


    private void startVoiceRecorder() {
        if (mVoiceRecorder != null) {
            mVoiceRecorder.stop();
        }

        mVoiceRecorder = new VoiceRecorder(mVoiceCallback);
        mVoiceRecorder.start();
    }


    @Override
    protected void onStart() {
        super.onStart();

        // Prepare Cloud Speech API
        bindService(new Intent(this, SpeechService.class), mServiceConnection, BIND_AUTO_CREATE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_GRANTED) {
            startVoiceRecorder();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.RECORD_AUDIO)) {
            showPermissionMessageDialog();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO},
                    REQUEST_RECORD_AUDIO_PERMISSION);
        }
    }


    private void stopVoiceRecorder() {
        if (mVoiceRecorder != null) {
            mVoiceRecorder.stop();
            mVoiceRecorder = null;
        }
    }

    private AlertDialog alertDialog = null;

    private void dialogVoice() {
        alertDialog.show();
    }


    private void showStatus(final boolean hearingVoice) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                mStatus.setText(hearingVoice ? "Say something" : "Listening");
                mStatus.setTextColor(hearingVoice ? mColorHearing : mColorNotHearing);
//                imageView.setImageResource(hearingVoice ? R.drawable.okgoogle_on : R.drawable.okgoogle_off);
            }
        });
    }

    @Override
    public void onMessageDialogDismissed() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO},
                REQUEST_RECORD_AUDIO_PERMISSION);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
//                NavUtils.navigateUpFromSameTask(this);
                new StopProgress().execute();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
//        new StopProgress().execute();
        super.onStop();
    }

    private ProgressDialog progress;

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int result = textToSpeech.setLanguage(new Locale("id", "ID"));

            // tts.setPitch(5); // set pitch level

            // tts.setSpeechRate(2); // set speech speed rate

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("Language is not supported");
            } else {
//                speakOut("TEST");
                Toast.makeText(getApplicationContext(), "Silakan masukkan perintah Anda", Toast.LENGTH_LONG).show();
            }

        } else {
            Log.e("Initilization Failed");
        }

    }


    private void speakOut(String text) {
        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
//        textToSpeech.speak(text, TextToSpeech.QUEUE_ADD, null);
//        while (textToSpeech.isSpeaking()) {
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public class StopProgress extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progress = ProgressDialog.show(AssistantActivity.this, "HiBCA!",
                    "please wait....", true);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                // Stop listening to voice
                stopVoiceRecorder();

                // Stop Cloud Speech API
                mSpeechService.removeListener(mSpeechServiceListener);
                unbindService(mServiceConnection);
                mSpeechService = null;
            } catch (Exception ex) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            progress.dismiss();
            onBackPressed();
        }
    }

}
