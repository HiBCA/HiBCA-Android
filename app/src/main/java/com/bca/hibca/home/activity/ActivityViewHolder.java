package com.bca.hibca.home.activity;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bca.hibca.R;

/**
 * Created by Fandy Adam on 10/29/2017.
 */

public class ActivityViewHolder extends RecyclerView.ViewHolder  {

    public TextView textTitle;
    public TextView textDateCreated;
    public TextView textLastChat;
    public TextView textTimeLastChat;
    public TextView textStatus;
    public ImageView icon;

    public ActivityViewHolder(View view) {
        super(view);

        textTitle = (TextView) itemView.findViewById(R.id.textTitle);
        textDateCreated = (TextView) itemView.findViewById(R.id.textDateCreated);
        textLastChat = (TextView) itemView.findViewById(R.id.textLastChat);
        textTimeLastChat = (TextView) itemView.findViewById(R.id.textTimeLastChat);
        textStatus = (TextView) itemView.findViewById(R.id.textStatus);
        icon = (ImageView) itemView.findViewById(R.id.icon);
    }
}
