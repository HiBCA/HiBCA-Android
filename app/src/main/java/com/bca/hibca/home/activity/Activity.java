package com.bca.hibca.home.activity;

import android.graphics.drawable.Drawable;

import java.sql.Timestamp;

/**
 * Created by Fandy Adam on 10/29/2017.
 */

public class Activity {

    private String id;
    private String title;
    private String lastChat;
    private Drawable drawable;
    private String timeCreated;
    private String timeLastChat;
    private String status;
    private int type;


    public Activity() {
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    public String getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(String timeCreated) {
        this.timeCreated = timeCreated;
    }

    public String getTimeLastChat() {
        return timeLastChat;
    }

    public void setTimeLastChat(String timeLastChat) {
        this.timeLastChat = timeLastChat;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getLastChat() {
        return lastChat;
    }

    public void setLastChat(String lastChat) {
        this.lastChat = lastChat;
    }
}
