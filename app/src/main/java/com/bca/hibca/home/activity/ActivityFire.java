package com.bca.hibca.home.activity;

import android.graphics.drawable.Drawable;

/**
 * Created by Fandy Adam on 10/29/2017.
 */

public class ActivityFire {

    private String id;
    private String title;
    private String datecreate;
    private String status;
    private int type;

    public ActivityFire() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDatecreate() {
        return datecreate;
    }

    public void setDatecreate(String datecreate) {
        this.datecreate = datecreate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
