package com.bca.hibca.home.subcategory;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bca.hibca.R;

import java.util.List;

/**
 * Created by Fandy Adam on 7/27/2017.
 */

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryViewHolder> {

    private List<SubCategory> list;

    public SubCategoryAdapter(List<SubCategory> list) {
        this.list = list;
    }

    @Override
    public SubCategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_subcategory, parent, false);
        SubCategoryViewHolder viewHolder = new SubCategoryViewHolder(layoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SubCategoryViewHolder holder, int position) {
        holder.textTitle.setText(list.get(position).getTitle());
        holder.textDescription.setText(list.get(position).getDescription());
        holder.imageMenu.setImageDrawable(list.get(position).getDrawable());
    }

    @Override
    public int getItemCount() {
        return this.list.size();
    }
}