/*
 * Copyright 2015 Rudson Lima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bca.hibca.home.category;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Html;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bca.hibca.R;
import com.bca.hibca.assistant.AssistantActivity;
import com.bca.hibca.chat.ChatMessageActivity;
import com.bca.hibca.home.BackHandledFragment;
import com.bca.hibca.home.HomePagerFragment;
import com.bca.hibca.home.activity.ActivityFragment;
import com.bca.hibca.home.subcategory.SubCategory;
import com.bca.hibca.home.subcategory.SubCategoryActivity;
import com.bca.hibca.promo.PromoActivity;
import com.bca.hibca.util.BCAUtil;
import com.bca.hibca.util.ChatUtil;
import com.bca.hibca.util.Log;
import com.bca.hibca.util.RecyclerItemClickListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class CategoryFragment extends BackHandledFragment {

    private DatabaseReference mFirebaseDatabaseReference;
    private List<Category> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private CategoryAdapter mAdapter;
    private String MESSAGES_CHILD = "category";
    private HomePagerFragment pagerFragment;

    public void setPagerFragment(HomePagerFragment pagerFragment) {
        this.pagerFragment = pagerFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);


        StaggeredGridLayoutManager mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mLayoutManager.setAutoMeasureEnabled(false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);

        mAdapter = new CategoryAdapter(list);
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (list.get(position).getId().equals("others")) {
                            // custom dialog
                            final Dialog dialog = new Dialog(view.getContext());
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.dialog_menu);
                            dialog.setTitle("");

                            ((TextView) dialog.findViewById(R.id.title)).setText("Others");
                            ((TextView) dialog.findViewById(R.id.description)).setText(Html.fromHtml(list.get(position).getItem()));

                            LinearLayout linear1 = (LinearLayout) dialog.findViewById(R.id.linear1);
                            linear1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    pagerFragment.refresh();

                                    int randInt = new BCAUtil().randInt(100, 10000);
                                    Intent intent = new Intent(v.getContext(), ChatMessageActivity.class);
                                    intent.putExtra("issueId", "issue" + randInt);
                                    intent.putExtra("issueTitle", "Others");
                                    startActivity(intent);
                                }
                            });

                            LinearLayout linear2 = (LinearLayout) dialog.findViewById(R.id.linear2);
                            linear2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    pagerFragment.refresh();

                                    int randInt = new BCAUtil().randInt(100, 10000);
                                    Intent intent = new Intent(v.getContext(), ChatMessageActivity.class);
                                    intent.putExtra("issueId", "issue" + randInt);

                                    intent.putExtra("issueTitle", "Others");
                                    startActivity(intent);
                                }
                            });

                            LinearLayout linear3 = (LinearLayout) dialog.findViewById(R.id.linear3);
                            linear3.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    pagerFragment.refresh();

                                    int randInt = new BCAUtil().randInt(100, 10000);
                                    Intent intent = new Intent(v.getContext(), ChatMessageActivity.class);
                                    intent.putExtra("issueId", "issue" + randInt);
                                    intent.putExtra("issueTitle", "Others");
                                    startActivity(intent);
                                }
                            });

                            dialog.show();
                        } else {
                            pagerFragment.refresh();
                            Intent intent;
                            if (list.get(position).getId().equals("promo")) {
                                intent = new Intent(view.getContext(), PromoActivity.class);
                            } else {
                                intent = new Intent(view.getContext(), SubCategoryActivity.class);
                            }
                            intent.putExtra("id", list.get(position).getId());
                            intent.putExtra("title", list.get(position).getTitle());
                            startActivity(intent);
                        }
                    }
                }));

        prepareData();

        rootView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        return rootView;
    }

    private void prepareData() {
        Log.info("MESSAGES_CHILD : " + MESSAGES_CHILD);
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseDatabaseReference.child(MESSAGES_CHILD).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.info("onDataChange");

                for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                    String title = (String) messageSnapshot.child("title").getValue();
                    List<String> description = (ArrayList) messageSnapshot.child("description").getValue();

                    Log.i("test " + title);
                    Log.i("test " + description);

                    Category category = messageSnapshot.getValue(Category.class);
                    Drawable drawable;
                    try {
                        byte[] decodedString = Base64.decode(category.getImage_base64(), Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        drawable = new BitmapDrawable(getResources(), decodedByte);
                    } catch (Exception ex) {
                        drawable = ContextCompat.getDrawable(getContext(), R.mipmap.promo);
                        Log.e("error " + ex.getMessage());
                        ex.printStackTrace();
                    }
                    category.setDrawable(drawable);

                    StringBuilder stringBuilder = new StringBuilder();
                    try {
                        for (String string : category.getDescription()) {
                            if (category.getId().equals("others")) {
                                stringBuilder.append(string);
                            } else {
                                stringBuilder.append("\u2022 " + string);
                            }
                            stringBuilder.append("<br/>");
                        }
                    } catch (Exception ex) {
                        Log.e("error " + ex.getMessage());
                        ex.printStackTrace();
                    }
                    category.setItem(stringBuilder.toString());

                    Log.i("category " + category.getId());
                    Log.i("category " + category.getItem());
                    Log.i("category " + category.getImage_base64());
                    Log.i("category " + category.getDescription());
                    Log.i("category " + category.getTitle());
                    Log.i("category " + category.getDrawable());

                    list.add(category);
                }


                mAdapter.notifyDataSetChanged();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.info("The read failed: " + databaseError.getCode());
            }
        });

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Auto-generated method stub
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.hibca, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub

        switch (item.getItemId()) {
            case R.id.menu_assistant:
                Toast.makeText(getActivity(), R.string.add, Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getContext(), AssistantActivity.class));
                break;
        }
        return true;
    }

    @Override
    public String getTagText() {
        return pagerFragment.getTagActive();
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }
}
