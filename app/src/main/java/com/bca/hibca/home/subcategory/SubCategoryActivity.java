package com.bca.hibca.home.subcategory;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Html;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bca.hibca.R;
import com.bca.hibca.chat.ChatMessageActivity;
import com.bca.hibca.util.BCAContants;
import com.bca.hibca.util.BCAUtil;
import com.bca.hibca.util.ChatUtil;
import com.bca.hibca.util.Log;
import com.bca.hibca.util.RecyclerItemClickListener;
import com.bca.hibca.voice.BaseActivity;
import com.bca.hibca.voice.CallScreenActivityVideo;
import com.bca.hibca.voice.CallScreenActivityVoice;
import com.bca.hibca.voice.SinchService;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sinch.android.rtc.MissingPermissionException;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.bca.hibca.util.BCAContants.SINCH.VIDEO_CALL_ID;
import static com.bca.hibca.util.BCAContants.SINCH.VOICE_CALL_ID;


public class SubCategoryActivity extends BaseActivity implements SinchService.StartFailedListenerVoice,
        com.bca.hibca.video.SinchService.StartFailedListenerVideo {

    private List<SubCategory> list = new ArrayList<>();
    private DatabaseReference mFirebaseDatabaseReference;
    private RecyclerView recyclerView;
    private String MESSAGES_CHILD = "subcategory/";
    private ChatUtil chatUtil;

    private SubCategoryAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_subcategory);

        setTitle("Hi " + BCAContants.ACCOUNT.fullname);

        chatUtil = new ChatUtil();

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        final Bundle bundle = getIntent().getExtras();
        MESSAGES_CHILD = MESSAGES_CHILD + bundle.getString("id");

        StaggeredGridLayoutManager mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mLayoutManager.setAutoMeasureEnabled(false);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        final SubCategory subCategory = list.get(position);
                        subCategory.setId("" + System.currentTimeMillis());

                        // custom dialog
                        final Dialog dialog = new Dialog(view.getContext());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.dialog_menu);
                        dialog.setTitle("");

                        ((TextView) dialog.findViewById(R.id.title)).setText(subCategory.getTitle());
                        ((TextView) dialog.findViewById(R.id.description)).setText(Html.fromHtml(subCategory.getDescription()));

                        LinearLayout linear1 = (LinearLayout) dialog.findViewById(R.id.linear1);
                        linear1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                Intent intent = new Intent(getApplicationContext(), ChatMessageActivity.class);
                                intent.putExtra("issueId", subCategory.getId());
                                intent.putExtra("issueTitle", subCategory.getTitle());
                                startActivity(intent);
                            }
                        });

                        LinearLayout linear2 = (LinearLayout) dialog.findViewById(R.id.linear2);
                        linear2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                try {
                                    Call call = getSinchServiceInterface().callUser(BCAContants.HALO.agent_voice);
                                    if (call == null) {
                                        // Service failed for some reason, show a Toast and abort
                                        Toast.makeText(v.getContext(), "Service is not started. Try stopping the service and starting it again before "
                                                + "placing a call.", Toast.LENGTH_LONG).show();
                                        return;
                                    }

                                    chatUtil.pushNewActivity(2, subCategory.getTitle());

                                    String callId = call.getCallId();
                                    Intent intent = new Intent(getApplicationContext(), CallScreenActivityVoice.class);
                                    intent.putExtra("issueId", subCategory.getId());
                                    intent.putExtra("issueTitle", subCategory.getTitle());

                                    intent.putExtra("sTitle", bundle.getString("title"));
                                    intent.putExtra("sSubtitle", subCategory.getTitle());

                                    intent.putExtra(VOICE_CALL_ID, callId);
                                    startActivity(intent);
                                } catch (MissingPermissionException e) {
                                    ActivityCompat.requestPermissions(SubCategoryActivity.this, new String[]{e.getRequiredPermission()}, 0);
                                }

                            }
                        });

                        LinearLayout linear3 = (LinearLayout) dialog.findViewById(R.id.linear3);
                        linear3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                try {
                                    Call call = getSinchServiceInterface().callUserVideo(BCAContants.HALO.agent_video);
                                    if (call == null) {
                                        // Service failed for some reason, show a Toast and abort
                                        Toast.makeText(v.getContext(), "Service is not started. Try stopping the service and starting it again before "
                                                + "placing a call.", Toast.LENGTH_LONG).show();
                                        return;
                                    }

                                    chatUtil.pushNewActivity(3, subCategory.getTitle());

                                    String callId = call.getCallId();
                                    Intent intent = new Intent(getApplicationContext(), CallScreenActivityVideo.class);
                                    intent.putExtra("issueId", subCategory.getId());
                                    intent.putExtra("issueTitle", subCategory.getTitle());

                                    intent.putExtra("sTitle", bundle.getString("title"));
                                    intent.putExtra("sSubtitle", subCategory.getTitle());

                                    intent.putExtra(VIDEO_CALL_ID, callId);
                                    startActivity(intent);
                                } catch (MissingPermissionException e) {
                                    ActivityCompat.requestPermissions(SubCategoryActivity.this, new String[]{e.getRequiredPermission()}, 0);
                                }
                            }
                        });

                        dialog.show();
                    }
                }));

        mAdapter = new SubCategoryAdapter(list);
        recyclerView.setAdapter(mAdapter);

        prepareData();

        mSpinner = new BCAUtil().showSpinnerLoading(this);
    }


    private void prepareData() {
        Log.info("MESSAGES_CHILD : " + MESSAGES_CHILD);
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseDatabaseReference.child(MESSAGES_CHILD).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.info("onDataChange");

                JSONObject jsonObject = new ChatUtil().convertDataJson(dataSnapshot);
                for (Iterator<String> it = jsonObject.keys(); it.hasNext(); ) {
                    try {
                        String key = it.next();
                        Object value = jsonObject.get(key);

                        JSONObject obj = new JSONObject(value.toString());

                        SubCategory e = new SubCategory();
                        e.setId(obj.getString("id"));
                        e.setTitle(obj.getString("title"));
                        e.setImage_base64(obj.getString("image_base64"));

                        String description = obj.getString("description");
                        String splitDescription = description;
                        e.setDescription(splitDescription);

                        Drawable drawable;
                        try {
                            byte[] decodedString = Base64.decode(e.getImage_base64(), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            drawable = new BitmapDrawable(getResources(), decodedByte);
                        } catch (Exception ex) {
                            drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_deposit_products);
                            Log.e("error " + ex.getMessage());
                            ex.printStackTrace();
                        }
                        e.setDrawable(drawable);

                        list.add(e);
                    } catch (Exception e) {
                        Log.e(e.getMessage(), e);
                    }
                }

                mAdapter.notifyDataSetChanged();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.info("The read failed: " + databaseError.getCode());
            }
        });

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    //Voice
    @Override
    public void onStartFailedVoice(SinchError error) {
        Log.error(error.toString());
        Toast.makeText(this, "Connection is closed", Toast.LENGTH_LONG).show();
        if (mSpinner != null) {
            mSpinner.dismiss();
        }
    }

    @Override
    public void onStartedVoice() {
        isStartVoice = true;

        if (isStartVideo) {
            if (mSpinner != null) {
                mSpinner.dismiss();
            }
        }
    }

    @Override
    protected void onServiceConnected() {
        Toast.makeText(this, "Your phone service ready", Toast.LENGTH_LONG).show();
        getSinchServiceInterface().setStartListener(this);
        startClient();
    }

    private ProgressDialog mSpinner;

    private void startClient() {
        if (!getSinchServiceInterface().isStarted()) {
            getSinchServiceInterface().startClient(BCAContants.ACCOUNT.username);
            mSpinner.show();
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "You may now place a call", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "This application needs permission to use your microphone to function properly.", Toast
                    .LENGTH_LONG).show();
        }
    }


    //VIdeo Service

    @Override
    public void onStartFailedVideo(SinchError error) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();
        if (mSpinner != null) {
            mSpinner.dismiss();
        }
    }

    @Override
    public void onStartedVideo() {
        isStartVideo = true;

        if (isStartVoice) {
            if (mSpinner != null) {
                mSpinner.dismiss();
            }
        }
    }


    private boolean isStartVoice = false;
    private boolean isStartVideo = false;

    private boolean isStartFailedVoice = false;
    private boolean isStartFailedVideo = false;

}
