package com.bca.hibca.home.category;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bca.hibca.R;

import java.util.List;

/**
 * Created by Fandy Adam on 7/27/2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryViewHolder> {

    private List<Category> list;
    public CategoryAdapter(List<Category> list) {
        this.list = list;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_menu_new, parent, false);
        CategoryViewHolder viewHolder = new CategoryViewHolder(layoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        holder.textTitle.setText(list.get(position).getTitle());
        holder.textDescription.setText(Html.fromHtml(list.get(position).getItem()));
        holder.imageMenu.setImageDrawable(list.get(position).getDrawable());
    }

    @Override
    public int getItemCount() {
        return this.list.size();
    }
}