package com.bca.hibca.home;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bca.hibca.R;
import com.bca.hibca.home.activity.ActivityFragment;
import com.bca.hibca.home.category.CategoryFragment;
import com.bca.hibca.navigation.adapter.TabPagerItem;
import com.bca.hibca.navigation.adapter.ViewPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class HomePagerFragment extends BackHandledFragment {
    private List<TabPagerItem> mTabs = new ArrayList<>();

    @Override
    public String getTagText() {
        return "";
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        categoryFragment = new CategoryFragment();
        activityFragment = new ActivityFragment();

        categoryFragment.setPagerFragment(this);
        activityFragment.setPagerFragment(this);

        createTabPagerItem();
    }

    private CategoryFragment categoryFragment;
    private ActivityFragment activityFragment;

    private void createTabPagerItem() {


        mTabs.add(new TabPagerItem("New Activity", categoryFragment));
        mTabs.add(new TabPagerItem("Recent", activityFragment));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_viewpager, container, false);
        rootView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private int _pageNumber = 0;
    private ViewPager mViewPager;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mViewPager = (ViewPager) view.findViewById(R.id.viewPager);

        mViewPager.setOffscreenPageLimit(mTabs.size());
        final ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getChildFragmentManager(), mTabs);
        mViewPager.setAdapter(pagerAdapter);
        TabLayout mSlidingTabLayout = (TabLayout) view.findViewById(R.id.tabLayout);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mSlidingTabLayout.setElevation(15);
        }
        mSlidingTabLayout.setupWithViewPager(mViewPager);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            public void onPageSelected(int pageNumber) {
                _pageNumber = pageNumber;
            }

            public void onPageScrolled(int pageNumber, float arg1, int arg2) {
                // TODO Auto-generated method stub
                // Just define a callback method in your fragment and call it like this!
                if (pageNumber == 0) {
                    categoryFragment = (CategoryFragment) pagerAdapter.getItem(pageNumber);
                    activityFragment = (ActivityFragment) pagerAdapter.getItem(1);
                    activityFragment.refresh();
                } else if (pageNumber == 1) {
                    activityFragment = (ActivityFragment) pagerAdapter.getItem(pageNumber);
                    activityFragment.prepareData();
                }
            }

            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub

            }
        });

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (activityFragment != null) {
            activityFragment.refresh();
        }
    }

    public void refresh() {
        activityFragment.refresh();
    }

    public void scrollToTabFragment(int item) {
        mViewPager.setCurrentItem(item, true);
    }

    public String getTagActive() {
        if (_pageNumber == 1)
            return "ActivityFragment";
        else
            return "CategoryFragment";
    }
}