package com.bca.hibca.home.subcategory;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bca.hibca.R;

/**
 * Created by Fandy Adam on 10/29/2017.
 */

public class SubCategoryViewHolder extends RecyclerView.ViewHolder  {

    public TextView textTitle;
    public TextView textDescription;
    public ImageView imageMenu;

    public SubCategoryViewHolder(View view) {
        super(view);

        textTitle = (TextView) itemView.findViewById(R.id.textTitle);
        textDescription = (TextView) itemView.findViewById(R.id.textDescription);
        imageMenu = (ImageView) itemView.findViewById(R.id.imageMenu);
    }
}
