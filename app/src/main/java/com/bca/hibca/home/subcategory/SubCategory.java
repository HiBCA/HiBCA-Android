package com.bca.hibca.home.subcategory;

import android.graphics.drawable.Drawable;

/**
 * Created by Lincoln on 15/01/16.
 */
public class SubCategory {
    private String id;
    private String title;
    private String description;
    private String image_base64;
    private Drawable drawable;

    public SubCategory() {
    }

    public SubCategory(String id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage_base64() {
        return image_base64;
    }

    public void setImage_base64(String image_base64) {
        this.image_base64 = image_base64;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }
}
