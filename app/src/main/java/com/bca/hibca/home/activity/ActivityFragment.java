/*
 * Copyright 2015 Rudson Lima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bca.hibca.home.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.EditText;
import android.widget.Toast;

import com.bca.hibca.R;
import com.bca.hibca.assistant.AssistantActivity;
import com.bca.hibca.chat.ChatMessage;
import com.bca.hibca.chat.ChatMessageActivity;
import com.bca.hibca.chat.ChatMessageFragment;
import com.bca.hibca.home.BackHandledFragment;
import com.bca.hibca.home.HomePagerFragment;
import com.bca.hibca.util.BCAContants;
import com.bca.hibca.util.ChatUtil;
import com.bca.hibca.util.Log;
import com.bca.hibca.util.RecyclerItemClickListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;


public class ActivityFragment extends BackHandledFragment {


    private List<Activity> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private ActivityAdapter mAdapter;


    private DatabaseReference mFirebaseDatabaseReference;
    private String MESSAGES_CHILD = "activity/";

    private HomePagerFragment pagerFragment;

    public void setPagerFragment(HomePagerFragment pagerFragment) {
        this.pagerFragment = pagerFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View rootView = inflater.inflate(R.layout.fragment_act, container, false);

        StaggeredGridLayoutManager mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mLayoutManager.setAutoMeasureEnabled(false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Activity e = list.get(position);
                        if (e.getType() == 1 || e.getType() == 0) {
                            Intent intent = new Intent(view.getContext(), ChatMessageActivity.class);
                            intent.putExtra("issueId", list.get(position).getId());
                            intent.putExtra("issueTitle", list.get(position).getTitle());
                            startActivity(intent);
                        }
                    }
                })
        );

        mAdapter = new ActivityAdapter(list);
        recyclerView.setAdapter(mAdapter);

        prepareData();

        rootView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        return rootView;
    }

    public void refresh() {
        list.clear();
    }

    public void prepareData() {
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseDatabaseReference
                .child(MESSAGES_CHILD + BCAContants.ACCOUNT.username)
                .limitToLast(10)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.info("Activity onDataChange");

                        HashMap<String, Object> hashMap = new ChatUtil().convertDataMap(dataSnapshot);
                        for (HashMap.Entry<String, Object> entry : hashMap.entrySet()) {
                            String key = entry.getKey();
                            Object value = entry.getValue();

                            final Activity e = new Activity();

                            try {
                                JSONObject obj = new JSONObject(value.toString());
                                e.setId(obj.getString("id"));
                                e.setType(obj.getInt("type"));
                                e.setStatus(obj.getString("status"));
                                e.setTitle(obj.getString("title"));
                                e.setTimeCreated(obj.getString("datecreate"));
                            } catch (JSONException ex) {
                                Log.info(ex.getMessage(), ex);
                            }

                            {
                                Query lastQuery = mFirebaseDatabaseReference.child("message/" + e.getId()).orderByKey().limitToLast(1);
                                lastQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        Log.info("masukkkkk :: " + new ChatUtil().convertDataMap(dataSnapshot));

                                        for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                                            Log.info("masukkkkk 1 1 1 1");
                                            ChatMessage message = messageSnapshot.getValue(ChatMessage.class);
                                            try {
                                                e.setTimeLastChat(message.getDatetime().substring(9, message.getDatetime().length() - 3));
                                            } catch (Exception ex) {
                                                e.setTimeLastChat(message.getDatetime());
                                            }
                                            e.setLastChat(message.getText());
                                        }
                                        list.add(e);
                                        mAdapter.notifyDataSetChanged();
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                    }
                                });
                            }

                        }

                        // Sorting
                        Collections.sort(list, new Comparator<Activity>() {
                            @Override
                            public int compare(Activity o1, Activity o2) {
                                return o1.getId().compareTo(o2.getId());
                            }
                        });

                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.info("The read failed: " + databaseError.getCode());
                    }
                });

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Auto-generated method stub
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.hibca, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub

        switch (item.getItemId()) {
            case R.id.menu_assistant:
                Toast.makeText(getActivity(), R.string.add, Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getActivity(), AssistantActivity.class));
                break;
        }
        return true;
    }


    @Override
    public String getTagText() {
        return pagerFragment.getTagActive();
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    public void gotToItemPager(int item) {
        pagerFragment.scrollToTabFragment(item);
    }

}
