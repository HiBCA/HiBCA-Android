package com.bca.hibca.home.activity;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bca.hibca.R;

import java.util.List;

/**
 * Created by Fandy Adam on 7/27/2017.
 */

public class ActivityAdapter extends RecyclerView.Adapter<ActivityViewHolder> {

    private List<Activity> list;
    private Context mContext;

    public ActivityAdapter(List<Activity> list) {
        this.list = list;
    }

    @Override
    public ActivityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_act, parent, false);
        mContext = layoutView.getContext();

        ActivityViewHolder viewHolder = new ActivityViewHolder(layoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ActivityViewHolder holder, int position) {
        holder.textTitle.setText(list.get(position).getTitle());
        String status = list.get(position).getStatus();
        try {
            if (status.equalsIgnoreCase("Closed")) {
                holder.textStatus.setText(list.get(position).getStatus());
                holder.textStatus.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_blue_dark));
            } else if (status.equalsIgnoreCase("Open")) {
                holder.textStatus.setText("");
                holder.textStatus.setTextColor(ContextCompat.getColor(mContext, android.R.color.black));
            } else {
                holder.textStatus.setText(list.get(position).getStatus());
                holder.textStatus.setTextColor(ContextCompat.getColor(mContext, android.R.color.black));
            }
        } catch (Exception e) {
            holder.textStatus.setTextColor(ContextCompat.getColor(mContext, android.R.color.black));
        }
        holder.textDateCreated.setText(list.get(position).getTimeCreated());
        holder.textTimeLastChat.setText(list.get(position).getTimeLastChat());
        holder.textLastChat.setText(list.get(position).getLastChat());
        if (list.get(position).getType() == 1) {
            holder.icon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_message_icon));
        } else if (list.get(position).getType() == 2) {
            holder.icon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_voice_call_icon));
        } else if (list.get(position).getType() == 3) {
            holder.icon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_video_call_icon));
        } else {
            holder.icon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_message_icon));
        }
    }

    @Override
    public int getItemCount() {
            return this.list.size();
    }
}