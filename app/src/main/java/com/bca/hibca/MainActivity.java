/*
 * Copyright 2015 Rudson Lima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bca.hibca;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

import br.liveo.interfaces.OnItemClickListener;
import br.liveo.interfaces.OnPrepareOptionsMenuLiveo;
import br.liveo.model.HelpLiveo;
import br.liveo.navigationliveo.NavigationLiveo;

import com.bca.hibca.assistant.AssistantActivity;
import com.bca.hibca.chat.ChatMessageFragment;
import com.bca.hibca.home.BackHandledFragment;
import com.bca.hibca.home.HomePagerFragment;
import com.bca.hibca.home.activity.ActivityFragment;
import com.bca.hibca.home.category.CategoryFragment;
import com.bca.hibca.navigation.ui.activity.SettingsActivity;
import com.bca.hibca.navigation.ui.fragment.MainFragment;
import com.bca.hibca.util.BCAContants;
import com.bca.hibca.util.Config;
import com.bca.hibca.util.Log;
import com.bca.hibca.util.NotificationUtils;
import com.google.firebase.messaging.FirebaseMessaging;

public class MainActivity extends NavigationLiveo implements OnItemClickListener, BackHandledFragment.BackHandlerInterface {

    private HelpLiveo mHelpLiveo;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    public void onInt(Bundle savedInstanceState) {

        setTitle("Hi " + BCAContants.ACCOUNT.fullname);

        // User Information
        this.userName.setText(BCAContants.ACCOUNT.fullname);
        this.userEmail.setText(BCAContants.ACCOUNT.email);
        this.userPhoto.setImageResource(R.drawable.ic_bca_logo);
        this.userBackground.setImageResource(R.drawable.ic_user_background_first);

        // Creating items navigation
        mHelpLiveo = new HelpLiveo();
        mHelpLiveo.add("Home", R.drawable.ic_sidebar_home);
        mHelpLiveo.addSubHeader("Promo");
        mHelpLiveo.add("Message", R.drawable.ic_sidebar_chat, 7);
        mHelpLiveo.add("Phone Call", R.drawable.ic_sidebar_voice_call, 7);
        mHelpLiveo.add("Video Call", R.drawable.ic_sidebar_video_call, 7);
        mHelpLiveo.add("Manage Recorded", R.drawable.ic_sidebar_manage_recorded);
        mHelpLiveo.addSeparator(); //Item separator
        mHelpLiveo.add("About", R.drawable.ic_sidebar_about);
        mHelpLiveo.add("Setting", R.drawable.ic_sidebar_settings);
        mHelpLiveo.add("Logout", R.drawable.ic_sidebar_logout);

        //{optional} - Header Customization - method customHeader
        //View mCustomHeader = getLayoutInflater().inflate(R.layout.custom_header_user, this.getListView(), false);
        //ImageView imageView = (ImageView) mCustomHeader.findViewById(R.id.imageView);

        with(this).startingPosition(0) //Starting position in the list
                .addAllHelpItem(mHelpLiveo.getHelp())
                //{optional} - List Customization "If you remove these methods and the list will take his white standard color"
                //.selectorCheck(R.drawable.selector_check) //Inform the background of the selected item color
                //.colorItemDefault(R.color.nliveo_blue_colorPrimary) //Inform the standard color name, icon and counter
//                .colorItemSelected(R.color.nliveo_blue_colorPrimary) //State the name of the color, icon and meter when it is selected
                //.backgroundList(R.color.nliveo_black_light) //Inform the list of background color
                //.colorLineSeparator(R.color.nliveo_transparent) //Inform the color of the subheader line

                //{optional} - SubHeader Customization
                //.colorNameSubHeader(R.color.nliveo_blue_colorPrimary)
                //.colorLineSeparator(R.color.nliveo_green_colorPrimaryDark)

                .removeFooter()
//                .footerItem("Logout", R.drawable.ic_sidebar_logout)

                //{optional} - Second footer
                //.footerSecondItem(R.string.settings, R.drawable.ic_settings_black_24dp)

                //{optional} - Header Customization
                //.customHeader(mCustomHeader)

                //{optional} - Footer Customization
                //.footerNameColor(R.color.nliveo_blue_colorPrimary)
                //.footerIconColor(R.color.nliveo_blue_colorPrimary)

                //.footerSecondNameColor(R.color.nliveo_blue_colorPrimary)
                //.footerSecondIconColor(R.color.nliveo_blue_colorPrimary)

                //.footerBackground(R.color.nliveo_white)

                //{optional} - Remove color filter icon
                //.removeColorFilter()

                .setOnClickUser(onClickPhoto)
                .setOnPrepareOptionsMenu(onPrepare)
                .setOnClickFooter(onClickFooter)

                //{optional} - Second footer
                //.setOnClickFooterSecond(onClickFooter)
                .build();

        int position = this.getCurrentPosition();
//        this.setElevationToolBar(position != 2 ? 15 : 0);
        this.setElevationToolBar((position != 0) || (position != 2) ? 15 : 0);


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                }
            }
        };

        displayFirebaseRegId();
    }

    @Override
    public void onItemClick(int position) {
        Fragment mFragment = null;
        FragmentManager mFragmentManager = getSupportFragmentManager();

        switch (position) {
            case 0: {
                mFragment = new HomePagerFragment();
            }
            break;
            case 2: {
                ChatMessageFragment fragment = new ChatMessageFragment();
                fragment.setTypeIssue(1);
                mFragment = fragment;
            }
            break;
            case 3: {
                ChatMessageFragment fragment = new ChatMessageFragment();
                fragment.setTypeIssue(2);
                mFragment = fragment;
            }
            break;
            case 4: {
                ChatMessageFragment fragment = new ChatMessageFragment();
                fragment.setTypeIssue(3);
                mFragment = fragment;
            }
            break;
            case 9: {
                finish();
            }
            break;
            default:
                mFragment = MainFragment.newInstance(mHelpLiveo.get(position).getName());
                break;
        }

        if (mFragment != null) {
            mFragmentManager.beginTransaction().replace(R.id.container, mFragment).commit();
        }

        this.setElevationToolBar((position != 0) && (position != 2) ? 15 : 0);
    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        Log.e("Firebase reg id: " + regId);

    }

    private OnPrepareOptionsMenuLiveo onPrepare = new OnPrepareOptionsMenuLiveo() {
        @Override
        public void onPrepareOptionsMenu(Menu menu, int position, boolean visible) {
        }
    };

    private View.OnClickListener onClickPhoto = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Toast.makeText(getApplicationContext(), "onClickPhoto :D", Toast.LENGTH_SHORT).show();
            closeDrawer();
        }
    };

    private View.OnClickListener onClickFooter = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
            closeDrawer();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        } catch (Exception ex) {
        }
    }

    @Override
    public void onBackPressed() {
        Log.i(" onBackPressed : "+selectedFragment.getTagText()+" - "+selectedFragment.onBackPressed());

        if (selectedFragment == null || !selectedFragment.onBackPressed()) {
            // Selected fragment did not consume the back press event.
            if (selectedFragment.getTagText().equals("CategoryFragment")) {
                finish();
            } else if (selectedFragment.getTagText().equals("ActivityFragment")) {
                ActivityFragment fragment = (ActivityFragment) selectedFragment;
                fragment.gotToItemPager(0);
            }  else {
                super.onBackPressed();
            }
        }
    }

    private BackHandledFragment selectedFragment;

    @Override
    public void setSelectedFragment(BackHandledFragment selectedFragment) {
        this.selectedFragment = selectedFragment;
    }
}

