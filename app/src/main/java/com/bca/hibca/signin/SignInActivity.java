package com.bca.hibca.signin;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bca.hibca.MainActivity;
import com.bca.hibca.R;
import com.bca.hibca.util.Log;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.util.Locale;


public class SignInActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newlogin);

        Locale locale = new Locale("in", "ID");
        Locale.setDefault(locale);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Utils.getDatabase();

        TextView textView = (TextView) findViewById(R.id.text_continue);
        SpannableString content = new SpannableString(getResources().getString(R.string.continue_as_guest));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        textView.setText(content);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });


        Button buttonSignIn = (Button) findViewById(R.id.button_signin);
        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    DatabaseReference databaseReference;
    DatabaseReference root;

    private void upload() {
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.promo);

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://finhack-hibca.appspot.com");
        StorageReference mountainImagesRef = storageRef.child("images/" + System.currentTimeMillis() + ".jpg");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 20, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadTask = mountainImagesRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                Log.d("downloadUrl-->" + downloadUrl);
            }
        });

    }

    private void test() {
        String child = "activity";
        Log.info("FAN : ");
        root = FirebaseDatabase.getInstance().getReference(child);
//        {
//            String key = root.push().getKey();
//            // pushing user to 'users' node using the userId
//            root.child("12").setValue(new Testing("fandyadam"));
//            log(key+" - "+root);
//        }

        databaseReference = FirebaseDatabase.getInstance().getReference();
        Log.info("===============================");
        // Attach a listener to read the data at our posts reference
        databaseReference.child(child).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.info("onDataChange");
                append_chat_conversatin(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });
        Log.info("===============================");
        databaseReference.child(child).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.info("onChildAdded");
                append_chat_conversatin(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.info("onChildChanged");
                append_chat_conversatin(dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void append_chat_conversatin(DataSnapshot dataSnapshot) {
//        log("++++++++++++++++++++++++++++++++++++++++++++++++");
//        dataSnapshot.getChildren();
//        String xkey = (String) dataSnapshot.getKey();
//        String xvalue = (String) dataSnapshot.getValue();
//        log("key: " + xkey+ " | value : "+xvalue);

        Log.info("++++++++++++++++++++++++++++++++++++++++++++++++");
        int index = 0;
        Iterator<DataSnapshot> i = dataSnapshot.getChildren().iterator();
        while (i.hasNext()) {
            try {
                DataSnapshot snapshot = i.next();

                String key = snapshot.getKey();
                Object value = snapshot.getValue();
                Log.info("index : " + index + " | " + key + " - " + value);
                index++;
            } catch (Exception e) {
                Log.info("Error : " + e.getMessage());

            }
        }
    }

}

class Utils {
    private static FirebaseDatabase mDatabase;
    public static FirebaseDatabase getDatabase() {
        if (mDatabase == null) {
            mDatabase = FirebaseDatabase.getInstance();
            mDatabase.setPersistenceEnabled(true);
        }
        return mDatabase;
    }

}
