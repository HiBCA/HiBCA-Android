package com.bca.hibca.promo;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bca.hibca.R;

/**
 * Created by Fandy Adam on 10/29/2017.
 */

public class PromoViewHolder extends RecyclerView.ViewHolder  {

    public TextView textTitle;
    public TextView textDescription;
    public TextView textPeriod;
    public ImageView imagePromo;

    public PromoViewHolder(View view) {
        super(view);

        textTitle = (TextView) itemView.findViewById(R.id.textTitle);
        textDescription = (TextView) itemView.findViewById(R.id.textDescription);
        textPeriod = (TextView) itemView.findViewById(R.id.textPeriod);
        imagePromo = (ImageView) itemView.findViewById(R.id.imagePromo);
    }
}
