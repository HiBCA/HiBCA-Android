package com.bca.hibca.promo;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bca.hibca.R;
import com.bca.hibca.chat.ChatMessageActivity;
import com.bca.hibca.home.subcategory.SubCategory;
import com.bca.hibca.home.subcategory.SubCategoryAdapter;
import com.bca.hibca.util.ChatUtil;
import com.bca.hibca.util.Log;
import com.bca.hibca.util.RecyclerItemClickListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class PromoActivity extends AppCompatActivity {

    private List<Promo> list = new ArrayList<>();
    private DatabaseReference mFirebaseDatabaseReference;
    private RecyclerView recyclerView;
    private String MESSAGES_CHILD = "promo";
    private PromoAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_subcategory);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        Bundle bundle = getIntent().getExtras();

        StaggeredGridLayoutManager mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mLayoutManager.setAutoMeasureEnabled(false);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                    }
                }));

        mAdapter = new PromoAdapter(list);
        recyclerView.setAdapter(mAdapter);

        prepareData();
    }


    private void prepareData() {
        Log.info("MESSAGES_CHILD : "+MESSAGES_CHILD);
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseDatabaseReference.child(MESSAGES_CHILD).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> hashMap = new ChatUtil().convertDataMap(dataSnapshot);
                for (HashMap.Entry<String, Object> entry : hashMap.entrySet()) {
                    String key = entry.getKey();
                    Object value = entry.getValue();

                    Promo e = new Promo();
                    try {
                        JSONObject obj = new JSONObject(value.toString());

                        e.setId(obj.getString("id"));
                        e.setTitle(obj.getString("title"));
                        e.setDescription(obj.getString("description"));
                        e.setPeriod(obj.getString("period"));

                        Drawable drawable;
                        try {
                            e.setImage_base64(obj.getString("image_base64"));
                            byte[] decodedString = Base64.decode(e.getImage_base64(), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            drawable = new BitmapDrawable(getResources(), decodedByte);
                        } catch (Exception ex) {
                            drawable = ContextCompat.getDrawable(getApplicationContext(), R.mipmap.promo);
                            Log.e("error " + ex.getMessage());
                            ex.printStackTrace();
                        }
                        e.setDrawable(drawable);

                        list.add(e);
                    } catch (JSONException ex) {
                        Log.info(ex.getMessage(), ex);
                    }
                }

                mAdapter.notifyDataSetChanged();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.info("The read failed: " + databaseError.getCode());
            }
        });

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
