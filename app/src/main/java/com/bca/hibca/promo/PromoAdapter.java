package com.bca.hibca.promo;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bca.hibca.R;

import java.util.List;

/**
 * Created by Fandy Adam on 7/27/2017.
 */

public class PromoAdapter extends RecyclerView.Adapter<PromoViewHolder> {

    private List<Promo> list;
    private Context mContext;

    public PromoAdapter(List<Promo> list) {
        this.list = list;
    }

    @Override
    public PromoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_promo, parent, false);
        mContext = layoutView.getContext();

        PromoViewHolder viewHolder = new PromoViewHolder(layoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PromoViewHolder holder, int position) {
        holder.textTitle.setText(list.get(position).getTitle());
        holder.textDescription.setText(list.get(position).getDescription());
        holder.textPeriod.setText(list.get(position).getPeriod());
        holder.imagePromo.setImageDrawable(list.get(position).getDrawable());
    }

    @Override
    public int getItemCount() {
        return this.list.size();
    }
}