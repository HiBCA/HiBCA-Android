package com.bca.hibca.util;

import android.util.AndroidException;

/**
 * Created by Fandy Adam on 11/5/2017.
 */

public class Log {

    private final static String TAG = "HiBCA!";
    public final static int DEBUG = android.util.Log.DEBUG;
    public final static int ERROR = android.util.Log.ERROR;
    public final static int INFO = android.util.Log.INFO;
    public final static int VERBOSE = android.util.Log.VERBOSE;
    public final static int WARN = android.util.Log.WARN;

    public Log() {
    }

    public Log(String msg) {
        info(msg);
    }

    public Log(String msg, Throwable tr) {
        info(msg, tr);
    }

    public static void info(String msg) {
        android.util.Log.i(TAG, msg);
    }

    public static void info(String msg, Throwable tr) {
        android.util.Log.i(TAG, msg, tr);
    }

    public static void debug(String msg) {
        android.util.Log.d(TAG, msg);
    }

    public static void debug(String msg, Throwable tr) {
        android.util.Log.d(TAG, msg, tr);
    }

    public static void error(String msg) {
        android.util.Log.d(TAG, msg);
    }

    public static void error(String msg, Throwable tr) {
        android.util.Log.d(TAG, msg, tr);
    }

    public static void i(String msg) {
        android.util.Log.i(TAG, msg);
    }

    public static void i(String msg, Throwable tr) {
        android.util.Log.i(TAG, msg, tr);
    }

    public static void d(String msg) {
        android.util.Log.d(TAG, msg);
    }

    public static void d(String msg, Throwable tr) {
        android.util.Log.d(TAG, msg, tr);
    }

    public static void e(String msg) {
        android.util.Log.e(TAG, msg);
    }

    public static void e(String msg, Throwable tr) {
        android.util.Log.e(TAG, msg, tr);
    }

    public static void w(String msg) {
        android.util.Log.w(TAG, msg);
    }

    public static void w(String msg, Throwable tr) {
        android.util.Log.w(TAG, msg, tr);
    }

    public static void v(String msg) {
        android.util.Log.v(TAG, msg);
    }

    public static void v(String msg, Throwable tr) {
        android.util.Log.v(TAG, msg, tr);
    }
}
