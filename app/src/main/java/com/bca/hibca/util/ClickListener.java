package com.bca.hibca.util;

/**
 * Created by u058627 on 11/9/2017.
 */

import android.view.View;

public interface ClickListener {

    void onClick(View view, int position);

    void onLongClick(View view, int position);

}
