package com.bca.hibca.util;

import com.bca.hibca.home.activity.ActivityFire;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Fandy Adam on 11/5/2017.
 */

public class ChatUtil {

    private DatabaseReference databaseReference;

    public void initialize() {
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        databaseReference = FirebaseDatabase.getInstance().getReference();
    }

    public void getListActivityByUserId(String userId) {
        String child = "activity/" + userId;

//        databaseReference.child(child).addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                append_chat_conversatin(dataSnapshot);
//            }
//
//            @Override
//            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                append_chat_conversatin(dataSnapshot);
//            }
//
//            @Override
//            public void onChildRemoved(DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });

    }

    public List<HashMap<String, Object>> appendChatConversatin(DataSnapshot dataSnapshot) {
        List<HashMap<String, Object>> arrayList = new ArrayList<>();

        HashMap<String, Object> hashMap = new HashMap<>();

        Log.info("++++++++++++++++++++++++++++++++++++++++++++++++");
        int index = 0;
        Iterator<DataSnapshot> i = dataSnapshot.getChildren().iterator();
        while (i.hasNext()) {
            try {
                DataSnapshot snapshot = i.next();
                String key = snapshot.getKey();
                Object value = snapshot.getValue();

                Log.info("index : " + index + " | " + key + " - " + value);
                hashMap.put(key, value);

                index++;
            } catch (Exception e) {
                Log.info("Error : " + e.getMessage());
            }
        }

        return arrayList;
    }

    public HashMap<String, Object> convertDataMap(DataSnapshot dataSnapshot) {
        HashMap<String, Object> hashMap = new HashMap<>();

        Log.info("++++++++++++++++++++++++++++++++++++++++++++++++");
        int index = 0;
        Iterator<DataSnapshot> i = dataSnapshot.getChildren().iterator();
        while (i.hasNext()) {
            try {
                DataSnapshot snapshot = i.next();
                String key = snapshot.getKey();
                Object value = snapshot.getValue();

                Log.info("index : " + index + " | " + key + " - " + value);
                hashMap.put(key, value.toString());

                Object objVal = "";
                if(snapshot.getChildrenCount() > 0) {
                    Iterator<DataSnapshot> i_new = snapshot.getChildren().iterator();
                    JSONObject jsonObject_new = new JSONObject();
                    while (i_new.hasNext()) {
                        try {
                            DataSnapshot snapshot_new = i_new.next();
                            String key_new = snapshot_new.getKey();
                            Object value_new = snapshot_new.getValue();
                            jsonObject_new.put(key_new, value_new);
                        } catch (Exception e) {
                            Log.info("Error : " + e.getMessage());
                        }
                    }
                    objVal =  jsonObject_new.toString();
                } else {
                    objVal = value;
                }

                hashMap.put(key, objVal);
                Log.info("\t\t "+ objVal);

                index++;
            } catch (Exception e) {
                Log.info("Error : " + e.getMessage());
            }
        }

        return hashMap;
    }

    public JSONObject convertDataJson(DataSnapshot dataSnapshot) {
        JSONObject jsonObject = new JSONObject();

        Log.info("1w++++++++++++++++++++++++++++++++++++++++++++++++");
        int index = 0;
        Iterator<DataSnapshot> i = dataSnapshot.getChildren().iterator();
        while (i.hasNext()) {
            try {
                DataSnapshot snapshot = i.next();
                String key = snapshot.getKey();
                Object value = snapshot.getValue();

                Log.info("index : " + index + " | " + key + " - " + value);

                Object objVal = "";
                if(snapshot.getChildrenCount() > 0) {
                    System.out.println("masuk 1");
                    Iterator<DataSnapshot> i_new = snapshot.getChildren().iterator();
                    JSONObject jsonObject_new = new JSONObject();
                    while (i_new.hasNext()) {
                        try {
                            DataSnapshot snapshot_new = i_new.next();
                            String key_new = snapshot_new.getKey();
                            Object value_new = snapshot_new.getValue();
                            jsonObject_new.put(key_new, value_new);
                        } catch (Exception e) {
                            Log.info("Error : " + e.getMessage());
                        }
                    }
                    objVal =  jsonObject_new.toString();
                } else {
                    System.out.println("masuk 2");
                    objVal = value;
                }

                jsonObject.put(key, objVal);
                Log.info("\t\t "+ objVal);

                index++;
            } catch (Exception e) {
                Log.info("Error : " + e.getMessage());
            }
        }

        return jsonObject;
    }

    public void pushNewActivity(int type, String title) {
        DatabaseReference mFirebaseDatabaseReferenceActivity = FirebaseDatabase.getInstance().getReference();

        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
        ActivityFire e = new ActivityFire();
        e.setId(""+System.currentTimeMillis());
        e.setTitle(title);
        e.setStatus("Open");
        e.setDatecreate(format.format(new java.util.Date()));
        e.setType(type);
        mFirebaseDatabaseReferenceActivity.child("activity/"+BCAContants.ACCOUNT.username).push().setValue(e);
    }

    public void pushNewActivity(int type, String title, String id) {
        DatabaseReference mFirebaseDatabaseReferenceActivity = FirebaseDatabase.getInstance().getReference();

        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
        ActivityFire e = new ActivityFire();
        e.setId(id);
        e.setTitle(title);
        e.setStatus("Open");
        e.setDatecreate(format.format(new java.util.Date()));
        e.setType(type);
        mFirebaseDatabaseReferenceActivity.child("activity/"+BCAContants.ACCOUNT.username).push().setValue(e);
    }



}
