package com.bca.hibca.util;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.bca.hibca.voice.SinchService;
import com.sinch.android.rtc.MissingPermissionException;
import com.sinch.android.rtc.calling.Call;

import static android.content.Context.BIND_AUTO_CREATE;

/**
 * Created by Fandy Adam on 11/17/2017.
 */

public class VoiceUtil implements ServiceConnection {

    private SinchService.SinchServiceInterface mSinchServiceInterface;
    private Context context;

    public VoiceUtil(Context context) {
        this.context = context;
    }

    public void start() {
        context.bindService(new Intent(context, SinchService.class), this, BIND_AUTO_CREATE);
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (SinchService.class.getName().equals(componentName.getClassName())) {
            mSinchServiceInterface = (SinchService.SinchServiceInterface) iBinder;
            onServiceConnected();
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        if (SinchService.class.getName().equals(componentName.getClassName())) {
            mSinchServiceInterface = null;
            onServiceDisconnected();
        }
    }

    protected void onServiceConnected() {
        // for subclasses
    }

    protected void onServiceDisconnected() {
        // for subclasses
    }

    protected SinchService.SinchServiceInterface getSinchServiceInterface() {
        return mSinchServiceInterface;
    }

    public void setSinchServiceInterface(SinchService.SinchServiceInterface mSinchServiceInterface) {
        this.mSinchServiceInterface = mSinchServiceInterface;
    }

    public Call call(String userName) {
        Call call = null;
        try {
            call = getSinchServiceInterface().callUser(userName);
            if (call == null) {
                // Service failed for some reason, show a Toast and abort
                Toast.makeText(context, "Service is not started. Try stopping the service and starting it again before "
                        + "placing a call.", Toast.LENGTH_LONG).show();
            }
        } catch (MissingPermissionException e) {
            ActivityCompat.requestPermissions((Activity) context, new String[]{e.getRequiredPermission()}, 0);
        }
        return call;
    }

}
