package com.bca.hibca.util;

import com.bca.hibca.voice.SinchService;

/**
 * Created by Fandy Adam on 11/5/2017.
 */

public interface BCAContants {

    public static interface ACCOUNT {
        static String username = "bona";
        static String fullname = "Bonaventura Aditya";
        static String email = "bonaventura_aditya@bca.co.id";
    }

    public static interface HALO {
        static String agent_voice = "halobca";
        static String agent_video = "halobcavid";
    }

    public static interface SINCH {
        static final String APP_KEY = "400d5659-4185-4e3a-9fcd-e497e38ea8af";
        static final String APP_SECRET = "Y4Gk/6feiU+pnWVjrDrlNw==";
        static final String ENVIRONMENT = "sandbox.sinch.com";
        static final String VOICE_CALL_ID = "HIBCA_VOICE_CALL_ID";
        static final String VIDEO_CALL_ID = "HIBCA_VIDEO_CALL_ID";
    }


}
