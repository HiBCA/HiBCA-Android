package com.bca.hibca.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.bca.hibca.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.Random;

/**
 * Created by Fandy Adam on 11/17/2017.
 */

public class BCAUtil {

    public int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    public ProgressDialog showSpinner(Context context, String title, String message) {
        ProgressDialog mSpinner = new ProgressDialog(context);
        mSpinner.setTitle(title);
        mSpinner.setMessage(message);
        return mSpinner;
    }

    public ProgressDialog showSpinnerLoading(Context context) {
        ProgressDialog mSpinner = showSpinner(context, "", "Please wait ...");
        return mSpinner;
    }


    public void turnSpeaker(Context context) {
        Log.d("isOn : " + isSpeakerOn);
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (isSpeaker()) {
            audioManager.setMode(AudioManager.MODE_IN_CALL);
            audioManager.setMode(AudioManager.MODE_NORMAL);
        } else {
            //Seems that this back and forth somehow resets the audio channel
            audioManager.setMode(AudioManager.MODE_NORMAL);
            audioManager.setMode(AudioManager.MODE_IN_CALL);
        }
        isSpeakerOn = !isSpeakerOn;
        audioManager.setSpeakerphoneOn(isSpeakerOn);
    }

    private static boolean isSpeakerOn = false;
    private static boolean isMicrophoneOn = false;

    public boolean isSpeaker() {
        return isSpeakerOn;
    }

    public void enableSpeaker(Context context) {
        isSpeakerOn = true;
        turnSpeaker(context);
    }

    public void disableSpeaker(Context context) {
        isSpeakerOn = false;
        turnSpeaker(context);
    }

    public void turnMicrophone(Context context) {
        Log.d("isOn : " + isMicrophoneOn);
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        isMicrophoneOn = !isMicrophoneOn;
        audioManager.setMicrophoneMute(isMicrophoneOn);
    }

    public void uploadImage(Bitmap bitmap, OnFailureListener onFailureListener, OnSuccessListener onSuccessListener) {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://finhack-hibca.appspot.com");
        StorageReference mountainImagesRef = storageRef.child("images/" + System.currentTimeMillis() + ".png");


        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Bitmap resized = bitmap;
//        while(w > 600 || h > 600) {
//            resized = Bitmap.createScaledBitmap(bitmap,(int)(bitmap.getWidth()*0.6), (int)(bitmap.getHeight()*0.6), true);
//        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        resized.compress(Bitmap.CompressFormat.PNG, 90, baos);

        long size = resized.getByteCount();
        Log.i("before size : " + size);
        while (size > (1024) * 100) {
            Log.i("size : " + size);
            resized = Bitmap.createScaledBitmap(resized, (int) (resized.getWidth() * 0.8), (int) (resized.getHeight() * 0.8), true);
            resized.compress(Bitmap.CompressFormat.PNG, 50, baos);
            size = resized.getByteCount();
        }

        byte[] data = baos.toByteArray();
        UploadTask uploadTask = mountainImagesRef.putBytes(data);
        uploadTask
                .addOnFailureListener(onFailureListener)
                .addOnSuccessListener(onSuccessListener);

    }
}
