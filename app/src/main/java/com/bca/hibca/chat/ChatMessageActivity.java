package com.bca.hibca.chat;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bca.hibca.R;
import com.bca.hibca.home.activity.Activity;
import com.bca.hibca.home.activity.ActivityFire;
import com.bca.hibca.util.BCAContants;
import com.bca.hibca.util.BCAUtil;
import com.bca.hibca.util.ChatUtil;
import com.bca.hibca.util.ClickListener;
import com.bca.hibca.util.Log;
import com.bca.hibca.util.NotificationUtils;
import com.bca.hibca.util.RecyclerItemClickListener;
import com.bca.hibca.util.RecyclerViewItemClickListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.appindexing.Action;
import com.google.firebase.appindexing.FirebaseAppIndex;
import com.google.firebase.appindexing.FirebaseUserActions;
import com.google.firebase.appindexing.Indexable;
import com.google.firebase.appindexing.builders.Indexables;
import com.google.firebase.appindexing.builders.PersonBuilder;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import github.ankushsachdeva.emojicon.EmojiconEditText;
import github.ankushsachdeva.emojicon.EmojiconGridView;
import github.ankushsachdeva.emojicon.EmojiconsPopup;
import github.ankushsachdeva.emojicon.emoji.Emojicon;


public class ChatMessageActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {


    private String MESSAGES_CHILD = "message/";

    private SharedPreferences mSharedPreferences;
    private ImageView mSendButton;
    private RecyclerView mMessageRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private FirebaseRecyclerAdapter<ChatMessage, ChatMessageViewHolder> mFirebaseAdapter;
    private DatabaseReference mFirebaseDatabaseReference;
    private DatabaseReference mFirebaseDatabaseReferenceUpdate;
    private EditText mMessageEditText;
    private String username = BCAContants.ACCOUNT.username;
    private final int RESULT_LOAD_IMAGE = 101;
    private boolean isFirst = true;
    private ChatUtil chatUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        setTitle("Hi " + BCAContants.ACCOUNT.fullname);

        chatUtil = new ChatUtil();

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }


        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Bundle bundle = getIntent().getExtras();

        MESSAGES_CHILD = MESSAGES_CHILD + bundle.getString("issueId");
        setTitle(bundle.getString("issueTitle"));

        Log.i("ChatMessageActivity : " + MESSAGES_CHILD);

        mMessageRecyclerView = (RecyclerView) findViewById(R.id.messageRecyclerView);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setStackFromEnd(true);

        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseDatabaseReferenceUpdate = FirebaseDatabase.getInstance().getReference();

        {

            mFirebaseDatabaseReference.child(MESSAGES_CHILD).addValueEventListener(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    com.bca.hibca.util.Log.info("onDataChangexxxx");
                    new ChatUtil().convertDataMap(dataSnapshot);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });

        }

        SnapshotParser<ChatMessage> parser = new SnapshotParser<ChatMessage>() {
            @Override
            public ChatMessage parseSnapshot(DataSnapshot dataSnapshot) {
                try {
                    ChatMessage message = dataSnapshot.getValue(ChatMessage.class);
//                    if (message != null) {
//                        message.set(dataSnapshot.getKey());
//                    }
                    return message;
                } catch (Exception e) {
                    System.out.println("terjadi kesalahan : " + e.getMessage());
                    e.printStackTrace();
                    return new ChatMessage();
                }
            }
        };


        DatabaseReference messagesRef = mFirebaseDatabaseReference.child(MESSAGES_CHILD);

        FirebaseRecyclerOptions<ChatMessage> options =
                new FirebaseRecyclerOptions.Builder<ChatMessage>()
                        .setQuery(messagesRef, parser)
                        .build();

        mFirebaseAdapter = new FirebaseRecyclerAdapter<ChatMessage, ChatMessageViewHolder>(options) {

            @Override
            public int getItemViewType(int position) {
                if ((getItem(position).getUsername() + "").equals(username)) {
                    return 1;
                } else {
                    return 2;
                }
            }

            @Override
            public ChatMessageViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
                LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
                if (i == 1) {
                    return new ChatMessageViewHolder(inflater.inflate(R.layout.adapter_chat_right, viewGroup, false));
                } else {
                    return new ChatMessageViewHolder(inflater.inflate(R.layout.adapter_chat_left, viewGroup, false));
                }
            }

            @Override
            protected void onBindViewHolder(final ChatMessageViewHolder viewHolder,
                                            int position,
                                            ChatMessage friendlyMessage) {
                isFirst = false;
                try {
                    viewHolder.time.setText(friendlyMessage.getDatetime().substring(friendlyMessage.getDatetime().length() - 8, friendlyMessage.getDatetime().length() - 3));
                    viewHolder.mProgressBar.setVisibility(ProgressBar.VISIBLE);
                } catch (Exception ex) {
                }
                if (friendlyMessage.getType() == 0) {
                    if (friendlyMessage.isMasking()) {
                        viewHolder.messageTextView.setText("*******");
                    } else {
                        viewHolder.messageTextView.setText(friendlyMessage.getText());
                    }
                    viewHolder.messageImageView.setVisibility(ImageView.GONE);
                    viewHolder.messageTextView.setVisibility(TextView.VISIBLE);
                    viewHolder.mProgressBar.setVisibility(TextView.GONE);
                } else if (friendlyMessage.getType() == 1) {

                    if (friendlyMessage.isMasking()) {
                        viewHolder.messageTextView.setText("*******");

                        viewHolder.messageImageView.setVisibility(ImageView.GONE);
                        viewHolder.messageTextView.setVisibility(TextView.VISIBLE);
                        viewHolder.mProgressBar.setVisibility(TextView.GONE);
                    } else {

                        String imageUrl = friendlyMessage.getImageUrl();
                        Log.d("chat message image : " + imageUrl);

                        Glide.with(viewHolder.messageImageView.getContext())
                                .load(friendlyMessage.getImageUrl())
                                .listener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                        viewHolder.mProgressBar.setVisibility(View.GONE);
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                        viewHolder.mProgressBar.setVisibility(View.GONE);
                                        return false;
                                    }
                                })
                                .into(viewHolder.messageImageView);

                        viewHolder.messageImageView.setVisibility(ImageView.VISIBLE);
                        viewHolder.messageTextView.setVisibility(TextView.GONE);
                    }
                }

            }
        };


        mFirebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);

                int friendlyMessageCount = mFirebaseAdapter.getItemCount();
                int lastVisiblePosition = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                // If the recycler view is initially being loaded or the user is at the bottom of the list, scroll
                // to the bottom of the list to show the newly added message.
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (friendlyMessageCount - 1) && lastVisiblePosition == (positionStart - 1))) {
                    mMessageRecyclerView.scrollToPosition(positionStart);
                }

                Log.info("onItemRangeInserted : " + positionStart + " | " + itemCount);
//                // play notification sound
//                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
//                notificationUtils.playNotificationSound();
//                notificationUtils.showNotification(mFirebaseAdapter.getItem(itemCount-1).getText());
            }

        });

        mMessageRecyclerView.setLayoutManager(mLinearLayoutManager);
        mMessageRecyclerView.setAdapter(mFirebaseAdapter);

        mMessageRecyclerView.addOnItemTouchListener(new RecyclerViewItemClickListener(getApplicationContext(), mMessageRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                //triggers when click
            }

            @Override
            public void onLongClick(View view, final int position) {
                //triggers when you long press
                if (mFirebaseAdapter.getItem(position).isMasking()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setTitle("Chat Masking");
                    builder.setCancelable(false);
                    builder.setMessage("Are you sure want to unmask this chat?");
                    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Log.info("amananan " + mFirebaseAdapter.getRef(position).getKey());

                            dialogInputPIN(position, MESSAGES_CHILD, mFirebaseAdapter.getRef(position).getKey());
                        }
                    });
                    builder.setNegativeButton("NO", null);
                    builder.show();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setTitle("Chat Masking");
                    builder.setCancelable(false);
                    builder.setMessage("Are you sure want to mask this chat?");
                    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Log.info("amananan " + mFirebaseAdapter.getRef(position).getKey());
                            mFirebaseDatabaseReferenceUpdate
                                    .child(MESSAGES_CHILD)
                                    .child(mFirebaseAdapter.getRef(position).getKey())
                                    .child("masking").setValue(true);
                        }
                    });
                    builder.setNegativeButton("NO", null);
                    builder.show();
                }
            }
        }));

        mMessageEditText = (EditText) findViewById(R.id.messageEditText);
//        mMessageEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(mSharedPreferences
//                .getInt(CodelabPreferences.FRIENDLY_MSG_LENGTH, DEFAULT_MSG_LENGTH_LIMIT))});
        mMessageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    mSendButton.setEnabled(true);
                } else {
                    mSendButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        mSendButton = (ImageView) findViewById(R.id.buttonSend);
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFirst) {
                    Bundle bundle = getIntent().getExtras();
                    chatUtil.pushNewActivity(1, bundle.getString("issueTitle"), bundle.getString("issueId"));
                    isFirst = false;
                }

                SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
                ChatMessage friendlyMessage = new ChatMessage(
                        username,
                        mMessageEditText.getText().toString(),
                        "",
                        format.format(new java.util.Date()),
                        0,
                        false);
                mFirebaseDatabaseReference.child(MESSAGES_CHILD).push().setValue(friendlyMessage);
                mMessageEditText.setText("");
            }
        });


        final ImageView galleryButton = (ImageView) findViewById(R.id.gallery_btn);
        galleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

        final View rootView = findViewById(R.id.root_view);
        final ImageView emojiButton = (ImageView) findViewById(R.id.emoji_btn);
        final EditText emojiconEditText = mMessageEditText;

        // Give the topmost view of your activity layout hierarchy. This will be used to measure soft keyboard height
        final EmojiconsPopup popup = new EmojiconsPopup(rootView, this);

        //Will automatically set size according to the soft keyboard size
        popup.setSizeForSoftKeyboard();

        //If the emoji popup is dismissed, change emojiButton to smiley icon
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                changeEmojiKeyboardIcon(emojiButton, R.drawable.smiley);

            }
        });

        //If the text keyboard closes, also dismiss the emoji popup
        popup.setOnSoftKeyboardOpenCloseListener(new EmojiconsPopup.OnSoftKeyboardOpenCloseListener() {

            @Override
            public void onKeyboardOpen(int keyBoardHeight) {

            }

            @Override
            public void onKeyboardClose() {
                if (popup.isShowing())
                    popup.dismiss();
            }
        });

        //On emoji clicked, add it to edittext
        popup.setOnEmojiconClickedListener(new EmojiconGridView.OnEmojiconClickedListener() {

            @Override
            public void onEmojiconClicked(Emojicon emojicon) {
                if (mMessageEditText == null || emojicon == null) {
                    return;
                }

                int start = emojiconEditText.getSelectionStart();
                int end = emojiconEditText.getSelectionEnd();
                if (start < 0) {
                    emojiconEditText.append(emojicon.getEmoji());
                } else {
                    emojiconEditText.getText().replace(Math.min(start, end),
                            Math.max(start, end), emojicon.getEmoji(), 0,
                            emojicon.getEmoji().length());
                }
            }
        });

        //On backspace clicked, emulate the KEYCODE_DEL key event
        popup.setOnEmojiconBackspaceClickedListener(new EmojiconsPopup.OnEmojiconBackspaceClickedListener() {

            @Override
            public void onEmojiconBackspaceClicked(View v) {
                KeyEvent event = new KeyEvent(
                        0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                emojiconEditText.dispatchKeyEvent(event);
            }
        });

        // To toggle between text keyboard and emoji keyboard keyboard(Popup)
        emojiButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //If popup is not showing => emoji keyboard is not visible, we need to show it
                if (!popup.isShowing()) {

                    //If keyboard is visible, simply show the emoji popup
                    if (popup.isKeyBoardOpen()) {
                        popup.showAtBottom();
                        changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_action_keyboard);
                    }

                    //else, open the text keyboard first and immediately after that show the emoji popup
                    else {
                        emojiconEditText.setFocusableInTouchMode(true);
                        emojiconEditText.requestFocus();
                        popup.showAtBottomPending();
                        final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(emojiconEditText, InputMethodManager.SHOW_IMPLICIT);
                        changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_action_keyboard);
                    }
                }

                //If popup is showing, simply dismiss it to show the undelying text keyboard
                else {
                    popup.dismiss();
                }
            }
        });

        mSpinner = new BCAUtil().showSpinnerLoading(this);
    }


    private void dialogInputPIN(final int position, String mString1, String mString2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("PIN");
        builder.setCancelable(false);

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        input.setTransformationMethod(PasswordTransformationMethod.getInstance());
        int maxLength = 6;
        input.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLength)});

        builder.setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String m_Text = input.getText().toString();

                mFirebaseDatabaseReferenceUpdate
                        .child(MESSAGES_CHILD)
                        .child(mFirebaseAdapter.getRef(position).getKey())
                        .child("masking").setValue(false);

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private ProgressDialog mSpinner;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            try {
                mSpinner.show();

                Uri imageUri = data.getData();
                InputStream imageStream = getContentResolver().openInputStream(imageUri);
                Bitmap bitmap = BitmapFactory.decodeStream(imageStream);

                Log.i("picturePath  : " + imageUri);

                new BCAUtil().uploadImage(bitmap, new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                // Handle unsuccessful uploads
                                mSpinner.dismiss();
                                Log.d("downloadUrl--> error : " + exception.getMessage());

                                Toast.makeText(ChatMessageActivity.this, "Failed upload image", Toast.LENGTH_LONG).show();
                            }
                        }, new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                Log.d("downloadUrl--> success : " + downloadUrl);

                                SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
                                ChatMessage friendlyMessage = new ChatMessage(
                                        username,
                                        mMessageEditText.getText().toString(),
                                        downloadUrl.toString(),
                                        format.format(new java.util.Date()),
                                        1,
                                        false);
                                mFirebaseDatabaseReference.child(MESSAGES_CHILD).push().setValue(friendlyMessage);
                                mMessageEditText.setText("");

                                mSpinner.dismiss();

                                Toast.makeText(ChatMessageActivity.this, "Success upload image", Toast.LENGTH_LONG).show();
                            }
                        }
                );


            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
            }

        }
    }

    private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId) {
        iconToBeChanged.setImageResource(drawableResourceId);
    }


//    private Action getMessageViewAction(ChatMessage friendlyMessage) {
//        return new Action.Builder(Action.Builder.VIEW_ACTION)
//                .setObject(String.valueOf(friendlyMessage.getType()), MESSAGE_URL.concat(friendlyMessage.getId()))
//                .setMetadata(new Action.Metadata.Builder().setUpload(false))
//                .build();
//    }


//    private Indexable getMessageIndexable(ChatMessage friendlyMessage) {
//        PersonBuilder sender = Indexables.personBuilder()
//                .setIsSelf(mUsername.equals(friendlyMessage.getName()))
//                .setName(friendlyMessage.getName())
//                .setUrl(MESSAGE_URL.concat(friendlyMessage.getId() + "/sender"));
//
//        PersonBuilder recipient = Indexables.personBuilder()
//                .setName(mUsername)
//                .setUrl(MESSAGE_URL.concat(friendlyMessage.getId() + "/recipient"));
//
//        Indexable messageToIndex = Indexables.messageBuilder()
//                .setName(friendlyMessage.getText())
//                .setUrl(MESSAGE_URL.concat(friendlyMessage.getId()))
//                .setSender(sender)
//                .setRecipient(recipient)
//                .build();
//
//        return messageToIndex;
//    }

    @Override
    public void onPause() {
        mFirebaseAdapter.stopListening();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mFirebaseAdapter.startListening();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d("onConnectionFailed:" + connectionResult);
    }


}
