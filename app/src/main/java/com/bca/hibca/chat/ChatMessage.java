package com.bca.hibca.chat;

/**
 * Created by Fandy Adam on 10/29/2017.
 */

public class ChatMessage {

    private String username;
    private String text;
    private String imageUrl;
    private String datetime;
    private int type;
    private boolean masking;


    public ChatMessage() {
    }

    public ChatMessage(String username, String text, String imageUrl, String datetime, int type, boolean masking) {
        this.username = username;
        this.text = text;
        this.imageUrl = imageUrl;
        this.datetime = datetime;
        this.type = type;
        this.masking = masking;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isMasking() {
        return masking;
    }

    public void setMasking(boolean masking) {
        this.masking = masking;
    }
}
