package com.bca.hibca.chat;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bca.hibca.R;

/**
 * Created by Fandy Adam on 10/29/2017.
 */

public class ChatMessageViewHolder extends RecyclerView.ViewHolder  {

    public TextView messageTextView;
    public TextView messengerTextView;
    public ImageView messageImageView;
    public ProgressBar mProgressBar;
    public TextView time;


    public ChatMessageViewHolder(View view) {
        super(view);

        messageTextView = (TextView) itemView.findViewById(R.id.text);
        messageImageView = (ImageView) itemView.findViewById(R.id.messengerImage);
        messengerTextView = (TextView) itemView.findViewById(R.id.messengerTextView);
        mProgressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
        time = (TextView) itemView.findViewById(R.id.time);

    }
}
