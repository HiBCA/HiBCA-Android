package com.bca.hibca.voice;

import com.bca.hibca.R;
import com.bca.hibca.util.BCAUtil;
import com.bca.hibca.util.Log;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallEndCause;
import com.sinch.android.rtc.calling.CallListener;

import android.media.AudioManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import static com.bca.hibca.util.BCAContants.SINCH.VOICE_CALL_ID;

public class CallScreenActivityVoice extends BaseActivity {

    private AudioPlayer mAudioPlayer;
    private Timer mTimer;
    private UpdateCallDurationTask mDurationTask;

    private String mCallId;

    private TextView mCallDuration;
    private TextView mCallState;
//    private TextView mCallerName;

    private BCAUtil bcaUtil;

    private class UpdateCallDurationTask extends TimerTask {

        @Override
        public void run() {
            CallScreenActivityVoice.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateCallDuration();
                }
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice);

        TextView textTitle = (TextView) findViewById(R.id.textTitle);
        TextView textSubtitle = (TextView) findViewById(R.id.textSubtitle);

        Bundle bundle = getIntent().getExtras();
        textTitle.setText(bundle.getString("sTitle"));
        textSubtitle.setText(bundle.getString("sSubtitle"));

        mAudioPlayer = new AudioPlayer(this);
        mCallDuration = (TextView) findViewById(R.id.textDuration);
//        mCallerName = (TextView) findViewById(R.id.remoteUser);
        mCallState = (TextView) findViewById(R.id.callState);
        Button endCallButton = (Button) findViewById(R.id.imageEndCall);

        endCallButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                endCall();
            }
        });
        mCallId = getIntent().getStringExtra(VOICE_CALL_ID);


        bcaUtil = new BCAUtil();

        LinearLayout linearSpeaker = (LinearLayout) findViewById(R.id.linearSpeaker);
        linearSpeaker.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                bcaUtil.turnSpeaker(view.getContext());
                turnSpeaker();
            }
        });

        LinearLayout linearMute = (LinearLayout) findViewById(R.id.linearMute);
        linearMute.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                bcaUtil.turnMicrophone(view.getContext());
                turnMicrophone();
            }
        });

    }

    private void turnSpeaker() {
        ImageView ic_loudspeaker = (ImageView) findViewById(R.id.ic_loudspeaker);
        if (bcaUtil.isSpeaker()) {
            ic_loudspeaker.setImageResource(R.drawable.ic_loudspeaker_selected);
        } else {
            ic_loudspeaker.setImageResource(R.drawable.ic_loudspeaker);
        }
    }

    private void turnMicrophone() {
//        ImageView ic_mute = (ImageView) findViewById(R.id.ic_mute);
//        if (bcaUtil.isSpeaker()) {
//            ic_mute.setImageResource(R.drawable.ic_mute_selected);
//        } else {
//            ic_mute.setImageResource(R.drawable.ic_mute);
//        }
    }


    @Override
    public void onServiceConnected() {
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.addCallListener(new SinchCallListener());
//            mCallerName.setText(call.getRemoteUserId());
            mCallState.setText(call.getState().toString());
        } else {
            Log.e("Started with invalid callId, aborting.");
            finish();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mDurationTask.cancel();
        mTimer.cancel();
    }

    @Override
    public void onResume() {
        super.onResume();
        mTimer = new Timer();
        mDurationTask = new UpdateCallDurationTask();
        mTimer.schedule(mDurationTask, 0, 500);
    }

    @Override
    public void onBackPressed() {
        // User should exit activity by ending call, not by going back.
    }

    private void endCall() {
        mAudioPlayer.stopProgressTone();
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.hangup();
        }
        finish();
    }

    private String formatTimespan(int totalSeconds) {
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        return String.format(Locale.US, "%02d:%02d", minutes, seconds);
    }

    private void updateCallDuration() {
        Log.info("updateCallDuration " + mCallId);
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            mCallDuration.setText(formatTimespan(call.getDetails().getDuration()));
        }
    }

    private class SinchCallListener implements CallListener {

        @Override
        public void onCallEnded(Call call) {
            CallEndCause cause = call.getDetails().getEndCause();
            Log.d("Call ended. Reason: " + cause.toString());
            mAudioPlayer.stopProgressTone();
            setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
            String endMsg = "Call ended: " + call.getDetails().toString();
            Toast.makeText(CallScreenActivityVoice.this, endMsg, Toast.LENGTH_LONG).show();
            endCall();
        }

        @Override
        public void onCallEstablished(Call call) {
            Log.d("Call established");
            mAudioPlayer.stopProgressTone();
            mCallState.setText(call.getState().toString());
            setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
        }

        @Override
        public void onCallProgressing(Call call) {
            Log.d("Call progressing");
            mAudioPlayer.playProgressTone();
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
            // Send a push through your push provider here, e.g. GCM
        }

    }
}
